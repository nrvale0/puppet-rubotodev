class rubotodev {
  include rubotodev::params

  require java
  
  if !defined_with_params( Package[$rubotodev::params::jruby_package], {ensure => present}) { 
    package { $rubotodev::params::jruby_package: 
      ensure => present,
    }
  }

  if !defined_with_params( 
    Package[$rubotodev::params::package], {ensure => present, provider => gem} ) {
      package { $rubotodev::params::package: 
        ensure => present, 
        provider => gem,
      }
  }

  include android

}
