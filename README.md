rubotodev

Install a ruboto dev environment:

* JDK
* JRuby
* ruboto gem
* Android SDK

Notes
-----

* Currently only Debian ::osfamily supported.
* Requires an as-of-yet unmerged pull request against puppetlabs-java(#26). 

Usage
-----
    include rubutodev

License
-------
Apache Software License, Version 2.0

Contact
-------
Nathan Valentine - nrvale0@gmail.com|nathan.valentine@venntech.net

Support
-------
Please log tickets and issues at our [Projects site](https://github.com/nrvale0/puppet-rubotodev).
