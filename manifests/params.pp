class rubotodev::params {
  case $::osfamily {
    'debian': {
      $jruby_package = 'jruby'
      $package = 'ruboto'
    }
    default: {fail("OS family ${::osfamily} not supported by this module!")}
  }
}
